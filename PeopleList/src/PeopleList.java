
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import java.awt.MouseInfo;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

class PersonFileAccess {
static String FL = "c:/tmp/people.txt";

public static void loadPersonListFromFile(){
//ArrayList<Person>
    Scanner linReader = null;
        try {
            linReader = new Scanner(new File(FL));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(PersonFileAccess.class.getName()).log(Level.SEVERE, null, ex);
        }

        while (linReader.hasNext())
        {
            String line = linReader.nextLine();
            System.out.println(line);
            ArrayList<String> list = new ArrayList<>();
            
            Arrays.asList(line.split(";"));
            for(int i=0;i<list.size();i++)
{
             System.out.println(list.get(i));
}

        }
        linReader.close();
        
    }      



    public static void savePersonListToFile(ArrayList<String> list) { 
       System.out.println("here");
       ArrayList<String> newList = new ArrayList<>();
       newList = list;
       for (int i=0; i<newList.size(); i++)  {
           System.out.println(newList.get(i));
    }
       try {
                PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(FL, false)));
           for (int i=0; i<newList.size(); i++)  {
          out.println(newList.get(i));
    }
                
                out.close();
            } catch (IOException e) {
                JOptionPane.showMessageDialog(null,
                        e.getMessage(),
                        "File writing error",
                        JOptionPane.ERROR_MESSAGE);
            }   
    }            
}

public class PeopleList extends javax.swing.JFrame {

    DefaultListModel<Person> peopleListModel = new DefaultListModel<>();
    int currentIndex = -1;

    public PeopleList() {
        initComponents();
    }


    
     @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pmDelete = new javax.swing.JPopupMenu();
        miDelete = new javax.swing.JMenuItem();
        jScrollPane1 = new javax.swing.JScrollPane();
        lstPeople = new javax.swing.JList<>();
        lblName = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        lblAge = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTextPane2 = new javax.swing.JTextPane();
        btnAdd = new javax.swing.JButton();
        btnUpdate = new javax.swing.JButton();
        lblIndex = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        tfName = new javax.swing.JTextField();
        tfAge = new javax.swing.JTextField();
        tfPostalcode = new javax.swing.JTextField();
        btSave = new javax.swing.JButton();
        btLoad = new javax.swing.JButton();

        miDelete.setText("Delete");
        miDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miDeleteActionPerformed(evt);
            }
        });
        pmDelete.add(miDelete);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        lstPeople.setModel(peopleListModel);
        lstPeople.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lstPeopleMouseClicked(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                lstPeopleMouseReleased(evt);
            }
        });
        jScrollPane1.setViewportView(lstPeople);

        lblName.setText("Name");

        jLabel2.setText("ID");

        lblAge.setText("Age");

        jScrollPane3.setViewportView(jTextPane2);

        btnAdd.setText("Add");
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });

        btnUpdate.setText("Update");
        btnUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateActionPerformed(evt);
            }
        });

        lblIndex.setText("-");

        jLabel1.setText("Postal code");

        btSave.setText("Save");
        btSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btSaveActionPerformed(evt);
            }
        });

        btLoad.setText("Load");
        btLoad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btLoadActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblName)
                            .addComponent(jLabel2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 60, Short.MAX_VALUE)
                        .addComponent(lblIndex, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(39, 39, 39))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addComponent(lblAge)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(tfAge, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(28, 28, 28)
                                        .addComponent(btnAdd))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(2, 2, 2)
                                        .addComponent(jLabel1)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(btnUpdate)
                                    .addComponent(tfName)
                                    .addComponent(tfPostalcode, javax.swing.GroupLayout.DEFAULT_SIZE, 81, Short.MAX_VALUE)))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addGap(28, 28, 28)
                                .addComponent(btSave)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(btLoad)
                                .addGap(12, 12, 12)))
                        .addGap(28, 28, 28))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(37, 37, 37)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(lblIndex, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblName)
                    .addComponent(tfName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 45, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblAge)
                    .addComponent(tfAge, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(39, 39, 39)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(tfPostalcode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(28, 28, 28)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAdd)
                    .addComponent(btnUpdate))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btSave)
                    .addComponent(btLoad))
                .addGap(28, 28, 28))
            .addGroup(layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 241, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        try {
            String name = tfName.getText();
            String ageStr = tfAge.getText();
            String postalcode = tfPostalcode.getText();
            int age = Integer.parseInt(ageStr);
            Person p = new Person(name, age, postalcode);
            peopleListModel.addElement(p);
            tfName.setText("");
            tfAge.setText("");
            tfPostalcode.setText("");
        } catch (NumberFormatException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this,
                    ex.getMessage(),
                    "Number format error",
                    JOptionPane.ERROR_MESSAGE);
        } catch (IllegalArgumentException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this,
                    ex.getMessage(),
                    "Input error",
                    JOptionPane.ERROR_MESSAGE);
        }


    }//GEN-LAST:event_btnAddActionPerformed

    private void lstPeopleMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lstPeopleMouseClicked
        int currentIndex = lstPeople.getAnchorSelectionIndex();
        
        if (currentIndex == -1) {
            lblIndex.setText("-");
            return;
        }
        Person p = peopleListModel.elementAt(currentIndex);
        lblIndex.setText(String.valueOf(currentIndex));
        tfName.setText(p.getName());
        tfAge.setText(p.getAge() + "");
        tfPostalcode.setText(p.getPostalcode());
    }//GEN-LAST:event_lstPeopleMouseClicked

    private void btnUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateActionPerformed
        int currentIndex = lstPeople.getAnchorSelectionIndex();
        if (currentIndex == -1) {
            return;
        }
        try {
            String name = tfName.getText();
            String ageStr = tfAge.getText();
            String postalcode = tfPostalcode.getText();            
            
            int age = Integer.parseInt(ageStr);
            Person p = new Person(name, age, postalcode);           
                        
            peopleListModel.setElementAt(p, currentIndex);
        } catch (NumberFormatException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this,
                    ex.getMessage(),
                    "Number format error",
                    JOptionPane.ERROR_MESSAGE);
        } catch (java.lang.IllegalArgumentException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this,
                    ex.getMessage(),
                    "Input error",
                    JOptionPane.ERROR_MESSAGE);
        }
       
        
    }//GEN-LAST:event_btnUpdateActionPerformed

    private void lstPeopleMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lstPeopleMouseReleased
    int currentIndex = lstPeople.getAnchorSelectionIndex();
        if (currentIndex == -1) {
            return;
        } 
    if (evt.getButton() == 3) {
 

        int x = (int) MouseInfo.getPointerInfo().getLocation().getX();
        int y = (int) MouseInfo.getPointerInfo().getLocation().getY();

pmDelete.show(this, x, y);


   
    }
    }//GEN-LAST:event_lstPeopleMouseReleased

    private void miDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miDeleteActionPerformed

    int currentIndex = lstPeople.getAnchorSelectionIndex();    

    String name = tfName.getText();
    String strAge = tfAge.getText();
    String postalcode = tfPostalcode.getText();  
    
    //String line1 = name + " is " + strAge + " y/o at " + postalcode;     
    String line1 = peopleListModel.getElementAt(currentIndex).toString();
    int n = JOptionPane.showConfirmDialog(this,
    "Do want want to delete <" + line1 +">?",
    "Question",
    JOptionPane.YES_NO_OPTION);
    if (n == 0) {
        int age = Integer.parseInt(strAge);    
        //Person p = new Person(name, age, postalcode);                                 
        peopleListModel.remove(currentIndex);
    }   
    }//GEN-LAST:event_miDeleteActionPerformed

    private void btSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btSaveActionPerformed
        ArrayList<String> list = new ArrayList<>();
       for (int i=0; i<peopleListModel.getSize(); i++)  {
           list.add(peopleListModel.getElementAt(i).toString1());
    }
             
        PersonFileAccess.savePersonListToFile(list);
    }//GEN-LAST:event_btSaveActionPerformed

    private void btLoadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btLoadActionPerformed
PersonFileAccess.loadPersonListFromFile();
    }//GEN-LAST:event_btLoadActionPerformed



    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(PeopleList.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(PeopleList.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(PeopleList.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(PeopleList.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new PeopleList().setVisible(true);
            }
        });
        
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btLoad;
    private javax.swing.JButton btSave;
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnUpdate;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTextPane jTextPane2;
    private javax.swing.JLabel lblAge;
    private javax.swing.JLabel lblIndex;
    private javax.swing.JLabel lblName;
    private javax.swing.JList<Person> lstPeople;
    private javax.swing.JMenuItem miDelete;
    private javax.swing.JPopupMenu pmDelete;
    private javax.swing.JTextField tfAge;
    private javax.swing.JTextField tfName;
    private javax.swing.JTextField tfPostalcode;
    // End of variables declaration//GEN-END:variables
}
