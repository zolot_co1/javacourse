public class Person {

    private String name;
    private int age;
    private String postalcode;
    
    public Person(String name, int age, String postalcode) {

        this.setName(name);
        this.setAge(age);
        this.setPostalcode(postalcode);
    }

    @Override
    public String toString() {
        return String.format("%s is %d y/o at %s", name, age, postalcode);
    }

    public String toString1() {
        return String.format("%s;%d;%s", name, age, postalcode);
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name.length() < 2 || name.length() >50 || name.contains(";")) {
            throw new IllegalArgumentException("Name must be between 1 and 50 characrets long");
        }
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if (age < 1 || age > 150) {
            throw new IllegalArgumentException("Age must be between 1 and 150");
        }
        this.age = age;
    }

    public String getPostalcode() {
        return postalcode;
    }

    public void setPostalcode(String postalcode) {
        this.postalcode = postalcode;
    }
}
